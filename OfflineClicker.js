const prefix = "[OfflineClicker]";

function log(text) {
    const string = prefix + text;
    console.log(string);
}

function load(str) {
    log("Loading Time");
    Game.mods.OfflineClicker.saveData = JSON.parse(str);
    const saveObject = Game.mods.OfflineClicker.saveData;
    if (saveObject!=null) {
        const now = new Date();
        const elapsed = now.getTime() - saveObject.lastSeen;
        log("Elapsed Time Epoch: "+elapsed);
        const seconds = elapsed / 1000;
        log("Elapsed Time Seconds: "+seconds);
        Game.cookies += Game.cookiesPsRaw * seconds;
        log("Cookies Earned during time: "+(Game.cookiesPsRaw * seconds));
    }
}

function save() {
    log("Saving Time");
    const saveObject = Game.mods.OfflineClicker.saveData = {};
    let datetime = new Date();
    saveObject.lastSeen = datetime.getTime();
    saveObject.lastCookies = Game.cookies;
    return JSON.stringify(saveObject);
}
const hooks = {
    load,
    save
}
Game.registerMod("OfflineClicker", hooks);